// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "Online/ShooterPlayerState.h"
#include "ShooterGameInstance.h"

AShooterGameState::AShooterGameState(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	NumTeams = 2;
	RemainingTime = 0;
	bTimerPaused = false;
	NumWeaponsPerTeam.Add(10);
	NumWeaponsPerTeam.Add(10);
}

void AShooterGameState::GetLifetimeReplicatedProps( TArray< FLifetimeProperty > & OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps( OutLifetimeProps );

	DOREPLIFETIME( AShooterGameState, NumTeams );
	DOREPLIFETIME( AShooterGameState, RemainingTime );
	DOREPLIFETIME( AShooterGameState, bTimerPaused );
	DOREPLIFETIME( AShooterGameState, TeamScores );
	DOREPLIFETIME( AShooterGameState, NumWeaponsPerTeam );
}

void AShooterGameState::GetRankedMap(int32 TeamIndex, RankedPlayerMap& OutRankedMap) const
{
	OutRankedMap.Empty();

	//first, we need to go over all the PlayerStates, grab their score, and rank them
	TMultiMap<int32, AShooterPlayerState*> SortedMap;
	for(int32 i = 0; i < PlayerArray.Num(); ++i)
	{
		int32 Score = 0;
		AShooterPlayerState* CurPlayerState = Cast<AShooterPlayerState>(PlayerArray[i]);
		if (CurPlayerState && (CurPlayerState->GetTeamNum() == TeamIndex))
		{
			SortedMap.Add(FMath::TruncToInt(CurPlayerState->Score), CurPlayerState);
		}
	}

	//sort by the keys
	SortedMap.KeySort(TGreater<int32>());

	//now, add them back to the ranked map
	OutRankedMap.Empty();

	int32 Rank = 0;
	for(TMultiMap<int32, AShooterPlayerState*>::TIterator It(SortedMap); It; ++It)
	{
		OutRankedMap.Add(Rank++, It.Value());
	}
	
}

void AShooterGameState::SetNumWeaponsPerTeam(int32 team, int32 weapons)
{
	if (Role == ROLE_Authority)
	{
		this->NumWeaponsPerTeam[team] = weapons;
	}
	else
	{
		ServerSetNumWeaponsPerTeam(team, weapons);
	}
}

bool AShooterGameState::ServerSetNumWeaponsPerTeam_Validate(int32 team, int32 weapons)
{
	return true;
}

void AShooterGameState::ServerSetNumWeaponsPerTeam_Implementation(int32 team, int32 weapons)
{
	UE_LOG(LogTemp, Log, TEXT("Finally reached server code."));
	this->NumWeaponsPerTeam[team] = weapons;

	if (Role != ROLE_Authority)
	{
		UE_LOG(LogTemp, Error, TEXT("Server is not server, dafuq?"));
	}
}

void AShooterGameState::RequestFinishAndExitToMainMenu()
{
	if (AuthorityGameMode)
	{
		// we are server, tell the gamemode
		AShooterGameMode* const GameMode = Cast<AShooterGameMode>(AuthorityGameMode);
		if (GameMode)
		{
			GameMode->RequestFinishAndExitToMainMenu();
		}
	}
	else
	{
		// we are client, handle our own business
		UShooterGameInstance* GameInstance = Cast<UShooterGameInstance>(GetGameInstance());
		if (GameInstance)
		{
			GameInstance->RemoveSplitScreenPlayers();
		}

		AShooterPlayerController* const PrimaryPC = Cast<AShooterPlayerController>(GetGameInstance()->GetFirstLocalPlayerController());
		if (PrimaryPC)
		{
			check(PrimaryPC->GetNetMode() == ENetMode::NM_Client);
			PrimaryPC->HandleReturnToMainMenu();
		}
	}

}
